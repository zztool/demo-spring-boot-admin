package com.zzz.demo.config;


import de.codecentric.boot.admin.server.config.AdminServerProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;

/**
 * @author zhangzz
 * @description zhangzz
 * @date 2021/9/17 17:02
 */
@Configuration
public class SecuritySecureConfig extends WebSecurityConfigurerAdapter {

    private final String contextPath;

    public SecuritySecureConfig(AdminServerProperties adminServerProperties) {
        this.contextPath = adminServerProperties.getContextPath();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // 跨域设置，SpringBootAdmin客户端通过 instances 注册，见 InstancesController
        http.csrf()
                .csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse())
                .ignoringAntMatchers(contextPath + "/instances");

        // 静态资源
        http.authorizeRequests().antMatchers(contextPath + "/assets/**").permitAll();
        // 所有请求必须通过认证
        http.authorizeRequests().anyRequest().authenticated();

        // 整合 spring-boot-admin-server-ui
        http.formLogin().loginPage("/login").permitAll();
        http.logout().logoutUrl("/logout").logoutSuccessUrl("/login");

        // 启用 basic 认证，SpringBootAdmin客户端使用的是 basic认证
        http.httpBasic();
    }
}
